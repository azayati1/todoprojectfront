import React, {useState, useEffect} from 'react';
import './App.css';
import TodoTable from "./components/todoTable";
import axios from 'axios';
import {Todo} from "./entities/Todo";
import Filter from "./components/Filter";
import {Button, Container, IconButton} from "@material-ui/core";
import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Switch from '@material-ui/core/Switch';
import TodoAddForm from "./components/todoAddForm";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        margin: {
            margin: theme.spacing(1),
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
    }),
);

function App() {
    const classes = useStyles();

    const [todos, setTodos] = useState<Todo[]>([]);
    const [todoAddFormOpen, setTodoAddFormOpen] = React.useState(false);

    const handleClickOpen = () => {
        setTodoAddFormOpen(true);
    };

    const handleClose = () => {
        setTodoAddFormOpen(false);
    };

    const handleAdd= (todo) => {
        axios.post<Todo[]>("http://127.0.0.1:8000/todo",todo)
            .then((res) => setTodos(prevState => [todo,...prevState]))
            .catch((err => console.log(err)))
    };
    useEffect(() => {
        axios.post<Todo[]>("http://127.0.0.1:8000/todos")
            .then((res) => setTodos(res.data))
            .catch((err => console.log(err)))
    }, [])


    const handleChange = (id, event: React.ChangeEvent<HTMLInputElement>) => {

        return axios.patch<Todo[]>("http://127.0.0.1:8000/todo?id=" + id,{'state':event.target.checked ? "1" : "0"})
            .then((res) => {
                setTodos(prevState => prevState.map(todo => {
                if (todo.id === id) {
                    event.target.checked ? todo.state = "0" : todo.state = "1";

                }

                    return todo;
            }))
            }
            )
            .catch((err => console.log(err)))

    };


    const columns = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'id', // accessor is the "key" in the data
            },
            {
                Header: 'Name',
                accessor: 'name', // accessor is the "key" in the data
            },
            {
                Header: 'Description',
                accessor: 'description', // accessor is the "key" in the data
            },
            {
                Header: 'State',
                accessor: 'state', // accessor is the "key" in the data
            },
            {
                header: 'toggleState',
                accessor: 'toggleState',
                Cell: (cell) => (<Switch
                    checked={cell.row.values.state == "1"}
                    onChange={(e) => handleChange(cell.row.values.id, e)}
                    inputProps={{'aria-label': 'secondary checkbox'}}
                />)
            },

            {
                header: 'edit',
                accessor: 'edit',
                Cell: (cell) => (<IconButton onClick={() => console.log(cell.row.values.id)} aria-label="delete"
                                             className={classes.margin}>
                    <EditIcon fontSize="large"/>
                </IconButton>)
            },

            {
                header: 'delete',
                accessor: 'delete',
                Cell: (cell) => (<IconButton onClick={() => console.log(cell.row.values.name)} aria-label="delete"
                                             className={classes.margin}>
                    <DeleteIcon fontSize="large"/>
                </IconButton>)
            },
        ],
        [todos]
    )
    const filterTodos = (filter) => {
        return axios.post<Todo[]>("http://127.0.0.1:8000/todos", filter)
            .then((res) => setTodos(res.data))
            .catch((err => console.log(err)))
    }
    return (
        <Container maxWidth="md">
            <Filter searchTodos={filterTodos}/>
            <Button variant="outlined" color="secondary" onClick={handleClickOpen} >
                Add
            </Button>
            <TodoAddForm open={todoAddFormOpen} handleClose={handleClose} handleAdd={handleAdd}/>
            <TodoTable columns={columns} data={todos}/>
        </Container>
    );
}

export default App;
