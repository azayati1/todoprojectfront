import React, {useState, useEffect, useContext} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Button} from "@material-ui/core";
import MyContext from "../utils/context";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& > *': {
                margin: theme.spacing(1),
                width: '25ch',
            },
            flexGrow: 1,
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

export default function Filter({searchTodos}) {

    const classes = useStyles();
    const [state, setState] = React.useState({
        name:'',
        state:'x'
    });

    const handleChange = (event) => {
        const value = event.target.value;
        setState({
            ...state,
            [event.target.name]: value
        });
    };

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <FormControl className={classes.formControl}>
                <TextField id="outlined-basic" label="Name" variant="outlined" value={state.name} name="name"
                           onChange={handleChange}/>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">State</InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={state.state}
                    onChange={handleChange}
                    label="State" name="state"
                >
                    <MenuItem value="x">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value={"1"}>Done</MenuItem>
                    <MenuItem value={"0"}>Pending</MenuItem>
                </Select>
            </FormControl>
            <Button variant="outlined" color="secondary" onClick={()=>searchTodos(state)}>
                Search
            </Button>
        </form>
    )
}